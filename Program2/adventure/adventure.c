//
// Created by wearingart on 4/30/16.
//

#include <errno.h>     // for errno
#include <unistd.h>    // for getpid
#include <sys/types.h> // for pid_t
#include <stdio.h>     // for fgets, fopen, fclose, fseek
#include <stdlib.h>    // for rand and srand
#include <sys/stat.h>  // for stat
#include <string.h>    // for strcpy, strcat
#include <time.h>      // for time

    const int TOTAL_ROOMS = 10;
    const int NUM_ROOMS = 7;
    const int START_ROOM = 0;
    const int MID_ROOM = 1;
    const int END_ROOM = 2;
    const int MAX_CONNECTIONS = 6;
    const int MIN_CONNECTIONS = 3;


struct Rooms
{
    char name[12];
    int isSelected;
    int isConnected;
    int type;
    int numConnections;
    int connections[7];
};

int randInt(int min, int max);


int main()
{
    pid_t pID = getpid();
    char buffer[512];
    char pidStr[15];
    char filepath[50];
    char file[100];
    const char *rooms[TOTAL_ROOMS];
    time_t clock;

    struct stat st = {0};
    int retVal;
    int i;
    int randomRoom;
    int randomConnections;
    int numSelected = 0;
    FILE *fp;


    struct Rooms Room[TOTAL_ROOMS];     // All rooms in this struct
    struct Rooms *SelectedRm[NUM_ROOMS];

    rooms[0] = "Earth";
    rooms[1] = "Galifrey";
    rooms[2] = "Miranda";
    rooms[3] = "Vulcan";
    rooms[4] = "Tatooine";
    rooms[5] = "Furya";
    rooms[6] = "Apokolips";
    rooms[7] = "Battleworld";
    rooms[8] = "Caprica";
    rooms[9] = "Dune";

    //generate all rooms as Room
    for (i = 0; i < TOTAL_ROOMS; i++)
    {
        strcpy(Room[i].name, rooms[i]);
        Room[i].isSelected = 0;
        Room[i].isConnected = 0;
        Room[i].numConnections = 0;
    }

    //seed random number generator
    srand((unsigned) time(&clock));

    //create string used for the path to the room files
    sprintf(pidStr, "%d", pID);
    strcpy(filepath, "./wilkibri.rooms.");
    strcat(filepath, pidStr);


    if (stat(filepath, &st) == -1) {

        //create the directory for room files
        retVal = mkdir(filepath, 0777);

        // check return value to see if operation successful
        if (retVal == -1)
        {
            // if not, send message to standard error
            fprintf(stderr, "Unable to create directory.\n");
            perror("Error: in main");
            exit(1);
        }
    }


    //select rooms for adventure
    do
    {
        //generate a random number between 0 and 9
        randomRoom = randInt(0, TOTAL_ROOMS - 1);

        //select the room if it has not been selected already
        if (Room[randomRoom].isSelected == 0)
        {

            SelectedRm[numSelected] = &Room[randomRoom];
            Room[randomRoom].isSelected = 1;

            for (i = 0; i < 7; i++)
            {
                Room[randomRoom].connections[i] = 0;
            }
            //TESTING
            for (i=0;i<7;i++)
            {
                printf("[%i] = %i\n", i, Room[randomRoom].connections[i]);
            }

            printf("%s\n", SelectedRm[numSelected]->name);
            printf("%i\n", SelectedRm[numSelected]->isSelected);

            //increase selection counter
            numSelected++;
        }

    } while (numSelected < NUM_ROOMS); //keep doing until room selected is no longer less than max num of rooms


    //create connections
    //generate a random number of connections from 3 to 6
    randomConnections = randInt(MIN_CONNECTIONS, MAX_CONNECTIONS);

    // get random room for START_ROOM
    randomRoom = randInt(0, NUM_ROOMS - 1);

    //SelectedRm[randomRoom]->numConnections = randomConnections;
    SelectedRm[randomRoom]->type = START_ROOM;
    SelectedRm[randomRoom]->isConnected = 1;
    printf("NAME: %s\nCONNECTIONS: %i\nTYPE: %i\n\n", SelectedRm[randomRoom]->name, randomConnections,SelectedRm[randomRoom]->type);

    int currentRoom = randomRoom;

    for (i=0; i < randomConnections; i++)
    {
        randomRoom = randInt(0, NUM_ROOMS - 1);
        if (SelectedRm[randomRoom]->connections[currentRoom] == 0 && SelectedRm[randomRoom]->name != SelectedRm[currentRoom]->name)
        {
            //SelectedRm[randomRoom]->isConnected = 1;
            SelectedRm[randomRoom]->connections[currentRoom] = 1;
            SelectedRm[randomRoom]->numConnections += 1;
            SelectedRm[currentRoom]->connections[randomRoom] = 1;
            SelectedRm[currentRoom]->numConnections += 1;
            printf("CONNECTED TO START: %s\n", SelectedRm[randomRoom]->name);
        }
        else
        {
            i--;
        }
    }

int j = 0;
    for (i=0;i<7;i++)
    {
        printf("%s\n", SelectedRm[i]->name);

        for (j=0;j<7;j++)
        {
            printf("[%i] = %i\n", j, SelectedRm[i]->connections[j]);
        }
    }

    //generate a random number of connections from 3 to 6
int currentConnectedRoom = 0;

    randomConnections = randInt(MIN_CONNECTIONS, MAX_CONNECTIONS);

    do
    {
        currentRoom = randInt(0, NUM_ROOMS - 1);
        currentConnectedRoom = SelectedRm[currentRoom]->isConnected;

    } while (currentConnectedRoom == 1);

    do
    {

        SelectedRm[currentRoom]->isConnected = 1;

        randomRoom = randInt(0, NUM_ROOMS - 1);

        randomConnections = randomConnections - SelectedRm[currentRoom]->numConnections;


        if (SelectedRm[randomRoom]->connections[currentRoom] == 0 && SelectedRm[randomRoom]->name != SelectedRm[currentRoom]->name)
        {
            //SelectedRm[randomRoom]->isConnected = 1;
            SelectedRm[randomRoom]->connections[currentRoom] = 1;
            SelectedRm[currentRoom]->connections[randomRoom] = 1;
            printf("CONNECTED TO %s: %s\n", SelectedRm[currentRoom]->name, SelectedRm[randomRoom]->name);
        }

    }while(randomConnections >= 0);

    for (i=0;i<7;i++)
    {
        printf("%s\n", SelectedRm[i]->name);

        for (j=0;j<7;j++)
        {
            printf("[%i] = %i\n", j, SelectedRm[i]->connections[j]);
        }
    }
    //create room files
    /*for (i = 0; i < NUM_ROOMS; i++)
    {

        //create string for file to create
        strcpy(file, filepath);
        strcat(file, "/");
        strcat(file, SelectedRm[numSelected]->name);

        sprintf(buffer, "ROOM NAME: %s\n", Room[randomRoom].name);
        printf("File: %s\n", file);
        fp = fopen(file, "w+");
        fprintf(fp, "%s\n", buffer);

        printf("Filepath: %s\n", filepath);
        fclose(fp);

    }*/

    return 0;
}



int randInt(int min, int max)
{   // returns a random integer between min and max, inclusive
    // (max - min + 1) is the range
    // + min sets the bottom of the range
    return rand() % (max - min + 1) + min;
}

